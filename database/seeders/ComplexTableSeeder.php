<?php

namespace Database\Seeders;

use App\Models\Barang;
use App\Models\ItemPenjualan;
use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use App\Models\Penjualan;
use App\Models\Pelanggan;
class ComplexTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $penjualan1 = Penjualan::create([
            'tgl' => Carbon::now()->toDateTimeString(),
            'kode_pelanggan' => Pelanggan::find(1)->id,
            'subtotal' => 0,
        ]);
        $selectedBarang = [
            [Barang::find(1)->id, 2],
            [Barang::find(2)->id, 4],
            [Barang::find(3)->id, 5],
        ];
        
        foreach ($selectedBarang as $selected) {
            ItemPenjualan::create([
                'penjualan_id' => $penjualan1->id,
                'barang_id' => $selected[0],
                'qty' => $selected[1],
            ]);
        }

        $daftarBarang = [];
        $daftarHarga = [];
        foreach ($selectedBarang as $selected) {
            $daftarBarang = Barang::select('nama,harga')->where('id', $selected[0]);
        }
        foreach ($daftarBarang as $daftar) {
            $daftarHarga = $daftar->harga ;
        }
    }
}
