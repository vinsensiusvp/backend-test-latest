<?php

namespace Database\Seeders;

use App\Models\Pelanggan;
use Illuminate\Database\Seeder;

class PelanggansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pelanggan::create([
            'nama'=> 'ANDI',
            'domisili'=> 'JAK-UT',
            'jk'=> 'PRIA',
        ]);
    }
}
