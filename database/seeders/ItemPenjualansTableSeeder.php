<?php

namespace Database\Seeders;

use App\Models\Barang;
use App\Models\ItemPenjualan;
use App\Models\Penjualan;
use Illuminate\Database\Seeder;

class ItemPenjualansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ItemPenjualan::create([
            'penjualan_id' => Penjualan::find(1)->id,
            'barang_id' => Barang::find(1)->id,
            'qty' => 2,
        ]);
    }
}
