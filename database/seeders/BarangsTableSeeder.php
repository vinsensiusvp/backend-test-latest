<?php

namespace Database\Seeders;

use App\Models\Barang;
use Illuminate\Database\Seeder;

class BarangsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Barang::create([
            'nama' => 'PEN',
            'kategori' => 'ATK',
            'harga' => 150000,
        ]);
        Barang::create([
            'nama' => 'PENCIL',
            'kategori' => 'ATK',
            'harga' => 20000,
        ]);
        Barang::create([
            'nama' => 'BOOK',
            'kategori' => 'ATK',
            'harga' => 5000,
        ]);
    }
}
