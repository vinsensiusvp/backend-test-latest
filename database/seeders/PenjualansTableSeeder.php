<?php

namespace Database\Seeders;

use App\Models\Pelanggan;
use App\Models\Penjualan;
use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Seeder;

class PenjualansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Penjualan::create([
            'tgl' => Carbon::now()->toDateTimeString(),
            'kode_pelanggan' => Pelanggan::find(1)->id,
            'subtotal' => 50000,
        ]);
    }
}
