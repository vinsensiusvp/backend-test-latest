<?php


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/','App\Http\Controllers\BarangController@ListBarang');

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// User Controller
// Route::post("/register", [UserController::class, "registerUser"]);
// Route::post('/register', 'App\Http\Controllers\UserController@registerUser');


// Route::post('/login', 'App\Http\Controllers\UserController@loginUser');

// Route::middleware('auth:api')->group(function () {

//     Route::get('/user', 'App\Http\Controllers\UserController@userDetail');
// });


Route::get('ListBarang', 'App\Http\Controllers\BarangController@ListBarang');


Route::get('ListPelanggan', 'App\Http\Controllers\PelangganController@ListPelanggan');


Route::resource('Barang', 'App\Http\Controllers\BarangController', [

    'except' => ['edit', 'create']
]);

Route::resource('Pelanggan', 'App\Http\Controllers\PelangganController', [
    'except' => ['edit', 'create']


]);
Route::resource('PenjualanBarang', 'App\Http\Controllers\PenjualanController', [
    'except' => ['edit', 'create']


]);
