<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    use HasFactory;


    protected $fillable = [
        'nama',
        'kategori',
        'harga',
    ];

    public function itemsPenjualan()
    {
        return $this->belongsToMany(Penjualan::class, 'item_penjualans');
    }
}
