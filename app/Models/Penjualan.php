<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Penjualan extends Model
{
    use HasFactory;

    protected $fillable = [
        'tgl',
        'kode_pelanggan',
        'subtotal',
    ];

    public function pelanggan()
    {
        return $this->belongsTo(Pelanggan::class);
    }

    public function itemsPenjualan()
    {
        return $this->belongsToMany(Barang::class, 'item_penjualans');
    }
}
