<?php

namespace App\Http\Controllers;

use App\Models\ItemPenjualan;
use Illuminate\Http\Request;

class ItemPenjualanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $nota = $request->id_nota;

        // $nota = "NOTA" + $this->generateIdNota();

        // $count = $request->
        // $books = $request->books;

        // Book records to be saved
        // $item_records = [];

        // return $request;

        // return $nota;
        // $test = $request->testing;
        // $data = array();
        // foreach ($books as $book) {
        //     if (!empty($book)) {
        //         $data[] = [
        //             'name' => $book,
        //         ];
        //     }
        // }
    }


    public function generateIdNota()
    {
        // do {
        $code = random_int(100000, 999999);
        // } while (Product::where("NOTA", "=", $code)->first());

        // return $numbernota;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ItemPenjualan  $itemPenjualan
     * @return \Illuminate\Http\Response
     */
    public function show(ItemPenjualan $itemPenjualan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ItemPenjualan  $itemPenjualan
     * @return \Illuminate\Http\Response
     */
    public function edit(ItemPenjualan $itemPenjualan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ItemPenjualan  $itemPenjualan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ItemPenjualan $itemPenjualan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ItemPenjualan  $itemPenjualan
     * @return \Illuminate\Http\Response
     */
    public function destroy(ItemPenjualan $itemPenjualan)
    {
        //
    }
}
