<?php

namespace App\Http\Controllers;

use App\Models\Penjualan;
use App\Models\ItemPenjualan;
use App\Models\Pelanggan;
use Illuminate\Http\Request;

use Illuminate\Support\Carbon;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class PenjualanController extends Controller
{
    public $success_status = 200;
    public $failed_status = 201;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $getDataPenjualan = Penjualan::all();


        if ($getDataPenjualan) {
            return response()->json(["status" => $this->success_status, "success" => true, "data" => $getDataPenjualan]);
        } else {
            return response()->json(["status" => $this->failed_status, "success" => false]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // $validator      =           Validator::make(
        //     $request->all(),
        //     [
        //         "kode_pelanggan"      => "required",
        //         "barang_id"           => "required",
        //         "qty"                 => "required"
        //     ]
        // );

        // if ($validator->fails()) {
        //     return response()->json(["validation_errors" => $validator->errors()]);
        // }
        $ldate = date('Y-m-d H:i:s');
        $post_array         =       array(


            'tgl' => $ldate,
            'kode_pelanggan' => $request->kode_pelanggan,
            'subtotal' => 0,
        );

        // $insert =  DB::insert("INSERT INTO penjualans (tgl,kode_pelanggan,subtotal) values ('$ldate','$request->kode_pelanggan',6969)");

        $post               =       Penjualan::create($post_array);

        $lastid = $post->id;

        $id = $request->barang_id;
        $foreachQty =  $request->qty;

        // return $foreachQty;
        // $tampung = [];

        foreach (array_combine($id, $foreachQty) as $id => $qty) {
            $tampung[] = [
                'penjualan_id' => $lastid,
                'barang_id' => $id,
                'qty' => $qty,
                'created_at' => $ldate,
                'updated_at' => $ldate

            ];
        }
        // return $tampung;
        $post_item_penjualan               =       ItemPenjualan::insert($tampung);

        $join =   DB::Select("SELECT a.id,b.barang_id,b.qty,c.harga,(b.qty * c.harga) as total_harga FROM penjualans a JOIN item_penjualans b ON a.id=b.penjualan_id JOIN barangs c ON b.barang_id = c.id
        WHERE a.id =$lastid ");

        $updateSubTotal = 0;
        foreach($join as $lastJoin)
        {
            // return $lastJoin; 
            $updateSubTotal += $lastJoin->total_harga;
        }

        // return $updateSubTotal;
        $Update = DB::update("UPDATE penjualans set subtotal = '$updateSubTotal' where id = $lastid") ;

        // if($)

        if ($Update) {
            return response()->json(["status" => $this->success_status, "success" => true, "data" => $Update]);
        } else {
            return response()->json(["status" => $this->failed_status, "success" => false, "message" => " data barang not created."]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Penjualan  $penjualan
     * @return \Illuminate\Http\Response
     */
    public function show(Penjualan $penjualan)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Penjualan  $penjualan
     * @return \Illuminate\Http\Response
     */
    public function edit(Penjualan $penjualan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Penjualan  $penjualan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Penjualan $penjualan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Penjualan  $penjualan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Penjualan $penjualan)
    {
        //
    }
}
