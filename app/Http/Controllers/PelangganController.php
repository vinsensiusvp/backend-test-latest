<?php

namespace App\Http\Controllers;

use App\Models\Pelanggan;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\DB;
class PelangganController extends Controller
{
    public $success_status = 200;
    public $failed_status = 201;


    public function index()
    {
        //Get all data 
        $user = Auth::user();
        // if (!is_null($user)) {

            $getDataPelanggan = Pelanggan::all();

            if ($getDataPelanggan) {
                return response()->json(["status" => $this->success_status, "success" => true, "data" => $getDataPelanggan]);
            } else {
                return response()->json(["status" => $this->failed_status, "success" => false]);
            }
        // } else {
        //     return response()->json(["status" => $this->failed_status, "message" => " invalid auth token"]);
        // }
    }


    public function ListPelanggan()
    {
        $getDataPelanggan =   DB::select("SELECT id,nama as item FROM pelanggans ");

        if ($getDataPelanggan) {
            return response()->json(["status" => $this->success_status, "success" => true, "data" => $getDataPelanggan]);
        } else {
            return response()->json(["status" => $this->failed_status, "success" => false]);
        }
    }

    public function store(Request $request)
    {
        //Insert pelanggan
        $user = Auth::user();
        // if (!is_null($user)) {

            $validator      =           Validator::make(
                $request->all(),
                [
                    "nama"       =>      "required",
                    "domisili"   =>      "required",
                    "jk"         =>      "required",
                ]
            );

            if ($validator->fails()) {
                return response()->json(["validation_errors" => $validator->errors()]);
            }

            $post_array         =       array(
                "nama"          =>      $request->nama,
                "domisili"      =>      $request->domisili,
                "jk"            =>      $request->jk,
            );

            $post               =       Pelanggan::create($post_array);

            if (!is_null($post)) {
                return response()->json(["status" => $this->success_status, "success" => true, "data" => $post]);
            } else {
                return response()->json(["status" => $this->failed_status, "success" => false, "message" => " data Pelanggan not created."]);
            }
        // } else {
        //     return response()->json(["status" => $this->failed_status, "message" => " invalid auth token"]);
        // }
    }





    public function show($id)
    {
        //
        $user       =           Auth::user();
        

        // if (!is_null($user)) {
            $getDataPelanggan       =           Pelanggan::where("id", $id)->where("id", $id)->first();


            if (!is_null($getDataPelanggan)) {
                return response()->json(["status" => $this->success_status, "success" => true, "data" => $getDataPelanggan]);
            } else {
                return response()->json(["status" => $this->failed_status, "success" => false, "message" => "Data Pelanggan tidak di temukan!"]);
            }
        // } else {
        //     return response()->json(["status" => $this->failed_status, "message" => " invalid auth token"]);
        // }
    }



    public function update(Request $request,  $id)
    {
        //Update Pelanggan
        $input          =           $request->all();
        $user           =           Auth::user();
        // if (!is_null($user)) {

            // return $id;

            // validation
            $validator      =       Validator::make($input, [
                "nama"       =>      "required",
                "domisili"   =>      "required",
                "jk"         =>      "required",

            ]);

            if ($validator->fails()) {
                return response()->json(["status" => $this->failed_status, "validation_errors" => $validator->errors()]);
            }


            $UpdateDataPelanggan       = Pelanggan::where('id', $id)->update($input);
            if ($UpdateDataPelanggan) {
                // return $barang;
                return response()->json(["status" => $this->success_status, "success" => true, "data" => $UpdateDataPelanggan, "params" => $input]);
            } else {
                return response()->json(["status" => $this->failed_status, "success" => false, "id" => $id, "params" => $input]);
            }

        // } else {
        //     return response()->json(["status" => $this->failed_status, "message" => " invalid auth token"]);
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pelanggan  $pelanggan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
