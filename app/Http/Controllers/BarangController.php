<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class BarangController extends Controller
{
    public $success_status = 200;
    public $failed_status = 201;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get all barang
        $user = Auth::user();
        // if (!is_null($user)) {

            $getDataBarang = Barang::all();

            // $getDataBarang =   DB::select("SELECT id,nama as item,kategori,harga,created_at,updated_at FROM barangs ");

            if ($getDataBarang) {
                return response()->json(["status" => $this->success_status, "success" => true, "data" => $getDataBarang]);
            } else {
                return response()->json(["status" => $this->failed_status, "success" => false]);
            }
        // } else {
        //     return response()->json(["status" => $this->failed_status, "message" => " invalid auth token"]);
        // }
        // return Barang::all();
    }

    public function ListBarang()
    {
        // Get all barang
        // $user = Auth::user();
        // if (!is_null($user)) {

            // $getDataBarang = Barang::all();

            $getDataBarang =   DB::select("SELECT id,nama as item FROM barangs ");

            if ($getDataBarang) {
                return response()->json(["status" => $this->success_status, "success" => true, "data" => $getDataBarang]);
            } else {
                return response()->json(["status" => $this->failed_status, "success" => false]);
            }
        // } else {
        //     return response()->json(["status" => $this->failed_status, "message" => " invalid auth token"]);
        // }
        // return Barang::all();
    }


    public function store(Request $request)
    {
        //Insert Barang
        $user = Auth::user();

        // if (!is_null($user)) {
            $validator      =           Validator::make(
                $request->all(),
                [
                    "nama"       =>      "required",
                    "kategori"   =>      "required",
                    "harga"      =>      "required",
                ]
            );

            if ($validator->fails()) {
                return response()->json(["validation_errors" => $validator->errors()]);
            }

            $post_array         =       array(
                "nama"          =>      $request->nama,
                "kategori"      =>      $request->kategori,
                "harga"         =>      $request->harga,
                // "user_id"       =>      $user->id
            );

            $post               =       Barang::create($post_array);

            if (!is_null($post)) {
                return response()->json(["status" => $this->success_status, "success" => true, "data" => $post]);
            } else {
                return response()->json(["status" => $this->failed_status, "success" => false, "message" => " data barang not created."]);
            }
        // } else {
        //     return response()->json(["status" => $this->failed_status, "message" => " invalid auth token"]);
        // }
    }


    public function show($id)
    {
        //
        $user       =           Auth::user();

        // if (!is_null($user)) {
            $getDataBarang       =           Barang::where("id", $id)->where("id", $id)->first();



            if (!is_null($getDataBarang)) {
                return response()->json(["status" => $this->success_status, "success" => true, "data" => $getDataBarang]);
            } else {
                return response()->json(["status" => $this->failed_status, "success" => false, "message" => "Data barang tidak di temukan!"]);
            }
        // } else {
        //     return response()->json(["status" => $this->failed_status, "message" => " invalid auth token"]);
        // }
    }


    public function update(Request $request,  $id)
    {
        //
        $input          =           $request->all();
        $user           =           Auth::user();
        // if (!is_null($user)) {

            // validation
            $validator      =       Validator::make($input, [
                "nama"         =>      "required",
                "kategori"     =>      "required",
                "harga"        =>      "required",

            ]);

            if ($validator->fails()) {
                return response()->json(["status" => $this->failed_status, "validation_errors" => $validator->errors()]);
            }

            // update post
            // $UpdateDataBarang       =           $barangs->update($input);
            $UpdateDataBarang       = Barang::where('id', $id)->update($input);
            if ($UpdateDataBarang) {
                // return $barang;
                return response()->json(["status" => $this->success_status, "success" => true, "data" => $UpdateDataBarang, "params" => $input]);
            } else {
                return response()->json(["status" => $this->failed_status, "success" => false, "id" => $id, "params" => $input]);
            }
        // } else {
        //     return response()->json(["status" => $this->failed_status, "message" => " invalid auth token"]);
        // }
    }





    public function destroy($barang)
    {
        //Delete data barang
        // if (!is_null($barang)) {
            // return $barang;
            $delete         =     Barang::where('id', $barang)->forceDelete();
            // dd($delete);

            // return $delete;

            if ($delete == true) {
                return response()->json(["status" => $this->success_status, "success" => true, "message" => "Success! data barang deleted"]);
            } else {
                return response()->json(["status" => $this->failed_status, "success" => false, "message" => "Error, data barang not deleted"]);
            }
        // } else {
        //     return response()->json(["status" => $this->failed_status, "success" => false, "message" => "Data barang not found"]);
        // }
    }
}
